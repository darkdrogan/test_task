const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const uuid = require('uuid');

const port = 8080;

const allRooms = {};

app.get('/');

// connect to socket
io.on('connection', (socket) => {
  let addedUser = false;
  let userRooms = [];

  // when user connect to chat
  socket.on('add user', (username, userRoom, rooms = [], userkey) => {

    if (addedUser) return;

    // set socket to store username and create user room
    socket.username = username;
    socket.userkey = userkey === 36 ? userkey : uuid(4);
    addedUser = true;
    socket.room = userRoom !== null ? userRoom : uuid(4);
    if (rooms === null) {
      userRooms.push(userRoom);
      socket.join(socket.room);
    }
    if (rooms !== null) {
      rooms.split(',').map(room => {
        if (room !== socket.room) {
          socket.join(room);
          userRooms.push(room);
          if (allRooms[ room ] && allRooms[ room ].users.find(user => user.userkey !== socket.userkey)) {
            allRooms[ room ].users.push({ username: socket.username, socketId: socket.id, userkey: socket.userkey });
          }
        }
      });
    }

    // create new prop in the store of rooms or add user
    if (!allRooms[ socket.room ]) allRooms[ socket.room ] = {
      users: [],
      roomName: socket.username + '\'s chat'
    };
    allRooms[ socket.room ].users.push({ username: socket.username, socketId: socket.id, userkey: socket.userkey });

    //whet user join to chat(aren't using now and need refactor client side for this)
    socket.broadcast.to(socket.room).emit('user joined', ({
      username: socket.username,
      room: socket.room
    }));

    // emit user room for set it on client side, and send update room-list
    socket.emit('user room', socket.room, socket.userkey);
    socket.broadcast.emit('rooms', allRooms);
    socket.emit('rooms', allRooms);
  });

  // when user try to join room
  socket.on('join room', (room) => {
    // if selected room is non-existing, then send failed connection and redirect to user room
    if (!allRooms[ room ]) {
      socket.emit('failed connect to room', socket.room);
      return;
    }
    if (allRooms[ room ].users.find(user => user.userkey === socket.userkey)) {
      return;
    }
    socket.join(room);
    allRooms[ room ].users.push({ username: socket.username, socketId: socket.id, userkey: socket.userkey });

    // after join and change information in rooms, update room-list for users
    socket.broadcast.emit('rooms', allRooms);
    socket.emit('rooms', allRooms);
  });

  // when user send new message
  socket.on('new message', (room, text) => {
    let time = (new Date()).toLocaleTimeString();
    const message = {
      username: socket.username,
      room: room,
      text: text,
      time: time
    };
    socket.broadcast.to(room).emit('new message', message);
    socket.emit('new message', message);
  });

  // when user leave room, it isn't realized on client side now
  socket.on('leave room', (room) => {
    socket.leave(room);
    allRooms[ room ].users = allRooms[ room ].users.filter(user => user.userkey !== socket.userkey);
    deleteEmptyRoom();
    userRooms = userRooms.filter(userRoom => userRoom !== room);
    if (userRooms.length === 0) {
      addedUser = false;
    }
    socket.broadcast.emit('rooms', allRooms);
    socket.emit('rooms', allRooms);

  });

  // when user disconnected, user leave rooms and notife all users
  socket.on('disconnect', () => {
    if (addedUser) {
      Object.keys(allRooms).map(room => {
        return allRooms[ room ].users = allRooms[ room ].users.filter(user => user.userkey !== socket.userkey);
      });

      // check empty rooms
      deleteEmptyRoom();

      socket.broadcast.emit('rooms', allRooms);
      socket.emit('rooms', allRooms);
    }
  });
});

http.listen(port, () => console.log(`server created and listen port: ${ port }`));

// if rooms is empty then it will be delete from room-list
const deleteEmptyRoom = () => {
  Object.keys(allRooms).map(room => {
    if (allRooms[ room ].users.length === 0) {
      delete allRooms[ room ];
    }
  });
};
