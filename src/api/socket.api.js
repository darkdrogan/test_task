import io from 'socket.io-client';
export const socket = io.connect('http://localhost:8080');

/**
 * Function listens about dynamic rooms from the server and signs callback function on updates.
 * Returns the room of the user, when attempt to join room was failed
 * Returns the new list of rooms, when user join/leaved to room or room isn't exist more.
 * @param{func} updateRooms - callback function
 */
export const subscribeToUpdateRooms = (updateRooms) => {
  let userKey;
  socket.on('user room', (username, userkey) => {
    userKey = userkey;
  });

  socket.on('failed connect to room', (room) => {
    updateRooms(room);
  });


  socket.on('rooms', (rooms) => {
    const roomArray = Object.keys(rooms).map(item => ({room: item, data: rooms[item]}));
    const userRoomArray = roomArray.filter(room => {
      const users = room.data.users;
      if(users.find(user => {
        return user.userkey === userKey;
      })) return true;
    });

    updateRooms(userRoomArray);
  });
};

/**
 * Joined new user on server.
 * @param{string} username
 * @param{string} userRoom
 * @param{array} roomsId
 * @param{string} userkey
 */
export const connectToChat = (username, userRoom, roomsId, userkey) => {
  socket.emit('add user', username, userRoom, roomsId, userkey);
};

/**
 * Function listen to update message-list from subscribed rooms.
 * @param {function} callback
 */
export const subscribeToMessage = (callback) => {
  socket.on('new message', ({room, username, text, time}) => {
    const message = {room, username, text, time};
    callback(message);
  });
};


/**
 * Function send message to selected chat.
 * @param{string} room - selected room
 * @param{text} text - text of message
 */
export const sendMessage = (room, text) => {
  socket.emit('new message', room, text);
};

/**
 * Function to try connect user to selected room.
 * @param{string} room - uuid selected room
 */
export const joinRoom = (room) => {
  socket.emit('join room', room);
};

/**
 * Function for leave room
 * @param{string} room - uuid room
 */
export const leaveRoom = (room) => {
  socket.emit('leave room', room);
};
