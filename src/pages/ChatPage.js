import React from 'react';
import PropTypes from 'prop-types';

import { ChatRooms } from '../сomponents/Chat/ChatRooms';
import { ChatUsers } from '../сomponents/Chat/ChatUsers';
import { ChatWindow } from '../сomponents/Chat/ChatWindow';
import { sendMessage } from '../api/socket.api';
import { VideoBroadcasting } from '../сomponents/Chat/VideoBroadcasting';
import './ChatPage.scss';

export const ChatPage = ({ rooms, selectedRoom, messages, selectRoom }) => {
  return (
    <div className="content">
      <VideoBroadcasting/>
      <div className="chat-info__wrap">
        <ChatRooms rooms={ rooms } selectRoom={ selectRoom } selectedRoom={selectedRoom}/>
        <ChatUsers rooms={ rooms } selectedRoom={ selectedRoom }/>
      </div>
      <ChatWindow selectedRoom={ selectedRoom } messages={ messages } sendMessage={ sendMessage }/>
    </div>
  )
};

ChatPage.propTypes = {
  selectRoom: PropTypes.func
};

ChatPage.propTypes = {
  rooms: PropTypes.array,
  selectedRoom: PropTypes.string,
  selectRoom: PropTypes.func,
  messages: PropTypes.array
};

