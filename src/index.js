import React from 'react';
import { render } from 'react-dom';
import { Router, Route, Switch } from 'react-router';
import { createBrowserHistory } from 'history';

import { App } from './сomponents/App';

const history = createBrowserHistory();

export const Index = () => {
  return (
    <Router history={ history }>
      <Switch>
        <Route exact path='/' component={ App }/>
        <Route path='/:chatId' component={ App }/>
      </Switch>
    </Router>
  )
};

render(<Index/>, document.getElementById('root'));