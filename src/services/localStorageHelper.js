export function loadLocalStorage () {
  const username = localStorage.getItem('username');
  const userRoom = localStorage.getItem('userRoom');
  const roomsId = localStorage.getItem('roomsId');
  const userKey = localStorage.getItem('userKey');
  return ({username, userRoom, rooms: roomsId, userKey});
}

export function saveUserKey (userKey) {
  localStorage.setItem('userKey', userKey);
}

export function saveUsername (username) {
  localStorage.setItem('username', username);
}

export function saveUserRoom (userRoom) {
  localStorage.setItem('userRoom', userRoom);
}

export function saveRooms (roomsId) {
  localStorage.setItem('roomsId', roomsId);
}

export function clearStorage () {
  localStorage.clear();
}
