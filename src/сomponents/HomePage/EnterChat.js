import React from 'react';
import PropTypes from 'prop-types';

import './EnterChat.scss';

export const EnterChat = ({sendUsername = f => f, isShown}) => {
  let _username;
  return (
    <div className={isShown ? 'overlay' : 'overlay none'}>
      <div className='login page'>
        <form className='form'
          onSubmit={ e => {
          e.preventDefault();
          sendUsername(_username.value);
          _username.value = '';
        }}>
          <label htmlFor="username" className='title'>Введите имя:</label>
          <input name='username' className='usernameInput' ref={input => _username = input} type="text" autoComplete='off'/>
        </form>
      </div>
    </div>
  );
};

EnterChat.propTypes = {
  sendUsername: PropTypes.func,
  isShown: PropTypes.bool
};