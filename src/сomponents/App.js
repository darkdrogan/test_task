import React from 'react';
import PropTypes from 'prop-types';

import { ChatPage } from '../pages/ChatPage';
import { EnterChat } from './HomePage/EnterChat';
import {
  connectToChat,
  subscribeToUpdateRooms,
  socket,
  subscribeToMessage,
  joinRoom
} from '../api/socket.api';
import './App.scss';
import {
  clearStorage,
  loadLocalStorage,
  saveRooms,
  saveUserKey,
  saveUsername,
  saveUserRoom
} from '../services/localStorageHelper';
import { ErrorConnection } from './Error/ErrorConnection';

export class App extends React.Component {

  constructor (props) {
    const { username, userRoom, rooms, userKey } = loadLocalStorage();
    const usersData = {
      username: username || '',
      userRoom: userRoom || '',
      userKey: userKey || '',
      rooms: [],
      connectedRoomId: rooms || [],
      selectedRoom: '',
      messages: {},
      connectionError: false
    };
    super(props);
    this.state = { ...usersData };
    this.joinToChatRoom = this.joinToChatRoom.bind(this);
    this.observerRooms = this.observerRooms.bind(this);
    this.observerMessages = this.observerMessages.bind(this);
    this.setSelectedRoom = this.setSelectedRoom.bind(this);
  }

  componentDidMount () {
    if (this.state.username) {
      this.joinToChatRoom(
        this.state.username,
        this.state.userRoom,
        this.state.connectedRoomId,
        this.state.userKey);
    }
    subscribeToUpdateRooms(this.observerRooms);
    subscribeToMessage(this.observerMessages);
  }

  // function for take update message-list from server
  observerMessages ({ room, time, username, text }) {
    this.setState(prevState => {
      let obj = {};
      let messageFromRoom = [];
      if (prevState.messages[ room ]) messageFromRoom = prevState.messages[ room ].map(item => item);
      obj[ room ] = [ ...messageFromRoom, { time, username, text } ];
      let objMessages = {};
      Object.assign(objMessages, prevState.messages, obj);
      return { ...prevState, messages: objMessages };
    })
  }

  // function for take update rooms-list from server or canceled attempt
  // connection to non-existing room
  observerRooms (data) {
    if (data instanceof Object) {
      saveRooms(data.map(room => room.room));
      if (data.length === 0) {
        clearStorage();
        this.setState(prevState => ({
          ...prevState,
          username: ''
        }))
      }
      this.setState(prevState => ({
        ...prevState,
        rooms: data
      }));
    } else {
      this.setState(prevState => ({
        ...prevState,
        selectedRoom: data
      }));
      this.props.history.push(`/${ data }`);
    }
  }

  // function to join user to room and select this room
  setSelectedRoom (room) {
    let selectRoom = room !== '' ? room : this.state.userRoom;
    joinRoom(selectRoom);
    this.props.history.push('/' + selectRoom);
    this.setState(prevState => ({
      ...prevState,
      selectedRoom: selectRoom
    }))
  }

  // join to chat
  joinToChatRoom (username, room = '', roomsId = [], userKey = '') {
    if (username.trim() !== '') {
      if (userKey !== '' && roomsId.length !== 0 && room !== '') {
        connectToChat(username, room, roomsId, userKey);
      } else {
        connectToChat(username);
      }
      socket.on('user room', (userRoom, userkey) => {
        saveUsername(username);
        saveUserRoom(userRoom);
        saveUserKey(userkey);
        const pathname = this.props.history.location.pathname.slice(1);
        //if pathname length equal roomName then try connect to this room or connect to user's room
        pathname.length === 36
          ? this.setSelectedRoom(pathname)
          : this.setSelectedRoom(userRoom);
        this.setState(prevState => ({
          ...prevState,
          userRoom: userRoom,
          username: username
        }));
      });
      socket.on('connect_error', (error) => {
        this.setState( prevState => ({
          ...prevState,
          username: '',
          connectionError: true
        }));
        socket.on('connect', () => {
          this.setState( prevState => ({
            ...prevState,
            connectionError: false
          }))
        });
      });
    }
  }

  render () {
    return (
      <div>
        <ErrorConnection connectionError={this.state.connectionError}/>
        <EnterChat sendUsername={ this.joinToChatRoom } isShown={ this.state.username === '' }/>
        <ChatPage
          rooms={ this.state.rooms }
          selectedRoom={ this.state.selectedRoom }
          messages={ this.state.messages[ this.state.selectedRoom ] }
          selectRoom={ this.setSelectedRoom }/>
      </div>
    )
  }

  componentWillUnmount () {
    socket.close();
  }
}

App.propTypes = {
  history: PropTypes.object
};
