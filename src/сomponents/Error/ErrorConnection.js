import React from 'react';
import PropTypes from 'prop-types';
import './ErrorConnection.scss';

export const ErrorConnection = ({connectionError}) => {
  return (
    <div className={connectionError ? 'error__wrapper' : 'error_wrapper none'}>
      <div className="error-connection">
        Something wrong.<br/>
        Connection failed.<br/>
        Try again later.<br/>
      </div>
    </div>
  )
};

ErrorConnection.propTypes = {
  connectionError: PropTypes.bool
};
