import React from 'react';
import PropTypes from 'prop-types';

import './ChatWindow.scss';

export const ChatWindow = ({ selectedRoom, sendMessage = f => f, messages = [] }) => {
  let _messageInput;
  const send = (e) => {
    e.preventDefault();
    sendMessage(selectedRoom, _messageInput.value);
    _messageInput.value = '';
  };
  return (
    <div className='chat__wrapper'>
      <div className='chat__messages-window'>
        <ul>
          { messages.map(message =>
            <li key={ message.time }>[{ message.time }]
              <span className='chat__username'> { message.username }</span>
              : { message.text }</li>) }
        </ul>
      </div>
      <div className='chat__users-input'>
        <form onSubmit={ e => send(e)}>
          <div className='input-group'>
            <input type="text" ref={ input => _messageInput = input } maxLength={300}/>
            <a className='send-button' onClick={e => send(e)}>Отправить</a>
          </div>
        </form>
      </div>
    </div>
  )
};

ChatWindow.propTypes = {
  selectedRoom: PropTypes.string,
  sendMessage: PropTypes.func,
  message: PropTypes.array
};
