import React from 'react';
import PropTypes from 'prop-types';

import './ChatRooms.scss';
import { leaveRoom } from '../../api/socket.api';

export const ChatRooms = ({ rooms, selectRoom, selectedRoom }) => {

  return (
    <div className='chat-rooms__wrapper'>
      <div className='chat-rooms__header'>Chatrooms:</div>
      <div className='chat-rooms__content'>
        <ul>
          { rooms.map(room =>
            <li
              key={ room.room }
              onClick={ e => {
                e.preventDefault();
                selectRoom(room.room);
              } }
            className={selectedRoom === room.room ? 'selected' : 'notSelected'}>
              { room.data.roomName }
              <span onClick={e => {
                e.preventDefault();
                e.stopPropagation();
                selectRoom('');
                leaveRoom(room.room);
              }}>X</span>
            </li>) }
        </ul>
      </div>
    </div>
  );
};

ChatRooms.propTypes = {
  selectRoom: PropTypes.func,
  selectedRoom: PropTypes.string,
  rooms: PropTypes.array
};