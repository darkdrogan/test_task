import React from 'react';
import PropTypes from 'prop-types';

import './ChatUsers.scss';

export const ChatUsers = ({ rooms, selectedRoom }) => {
  const room = rooms.find(room => room.room === selectedRoom);
  return (
    <div className='users__wrapper'>
      <div className="users__header">Users:</div>
      <div className="users__content">
        {room ? <ul>
          { room.data.users.map(user => <li key={ user.userkey }>{ user.username }</li>) }
        </ul> : <ul></ul>}
      </div>
    </div>
  )
};

ChatUsers.propTypes = {
  rooms: PropTypes.array,
  selectedRoom: PropTypes.string
};
